mod app;

use app::{App, Props, Msg};
use crate::app::physics::{MY_PADDLE_X, THEIR_PADDLE_X, PhysicsMessage, Scene, Computed};
use crate::app::rtc::BallPositionAndVelocity;

use web_sys::window;
use wasm_bindgen::{JsValue, JsCast, closure::Closure};
use wasm_logger::{init, Config};
use std::sync::mpsc::channel;
use std::rc::Rc;
use std::cell::RefCell;
use std::ops::Neg;
use yew::Callback;
use rapier2d::prelude::*;
use log::info;
use rand::Rng;


static NEW_GAME_VELOCITY: Real = 6.0;


fn main() {
    // Initialize logger
    init(Config::default());

    let (send_physics_message, receive_physics_messages) = channel();

    let send_physics_message_onphysicsmessage = send_physics_message.clone();
    let onphysicsmessage_cb: Rc<dyn Fn(PhysicsMessage)> = Rc::new(move |m| {
        send_physics_message_onphysicsmessage.send(m).unwrap();
    });
    let onphysicsmessage = Callback::Callback {
        cb: onphysicsmessage_cb,
        passive: None,
    };

    let graphics = yew::start_app_with_props::<App>(Props {
        onphysicsmessage,
    });

    let mut rigid_body_set = RigidBodySet::new();
    let mut collider_set = ColliderSet::new();

    let Scene {rigid_bodies, colliders} = Scene::initialize(&mut rigid_body_set, &mut collider_set);


    // Physics constants
    let gravity = vector![0.0, 0.0];
    let mut physics_pipeline = PhysicsPipeline::new();
    let mut island_manager = IslandManager::new();
    let mut broad_phase = BroadPhase::new();
    let mut narrow_phase = NarrowPhase::new();
    let mut impulse_joint_set = ImpulseJointSet::new();
    let mut multibody_joint_set = MultibodyJointSet::new();
    let mut ccd_solver = CCDSolver::new();
    let (collision_send, collision_recv) = crossbeam::channel::unbounded();
    let (contact_force_send, _contact_force_recv) = crossbeam::channel::unbounded();
    let event_handler = ChannelEventCollector::new(collision_send, contact_force_send);


    type OnAnimationFrame = Rc<RefCell<Option<Closure<dyn FnMut(JsValue)>>>>;

    // Rendering / Physics stepping loop variables
    let mut start_time: Option<Real> = None;
    // cb_inner is used within the callback, to keep requesting an animation frames
    let cb_inner: OnAnimationFrame = Rc::new(RefCell::new(None));
    // cb_outer is only used once, to initiate the request for an animation frame
    let cb_outer: OnAnimationFrame = cb_inner.clone();

    // Handle physics step per animation frame
    let window = window().unwrap();
    let window_cb = window.clone();
    let mut computed_state = Computed::new();
    let cb: Box<dyn FnMut(JsValue)> = Box::new(move |timestamp| {
        // Physics messages from the grpahics / network
        for physics_message in receive_physics_messages.try_iter() {
            match physics_message {
                PhysicsMessage::StartGame => {
                    let ball_body = rigid_body_set.get_mut(
                        rigid_bodies.ball_body_handle
                    ).unwrap();
                    ball_body.set_translation(vector![0.0, 0.0], true);
                    // FIXME: vector velocity from polar velocity, with random angle
                    // s.t. angle isn't within 30 degrees +/- vertical
                    let mut rng = rand::thread_rng();
                    let angle: Real = rng.gen_range(0.0..(std::f32::consts::PI / 6.0));
                    let vel_x: Real = angle.cos() * NEW_GAME_VELOCITY;
                    let vel_y: Real = angle.sin() * NEW_GAME_VELOCITY;
                    let up_or_down: bool = rng.gen();
                    let left_or_right: bool = rng.gen();
                    let vel = vector![
                        if left_or_right {vel_x.neg()} else {vel_x},
                        if up_or_down {vel_y} else {vel_y.neg()}
                    ];
                    ball_body.set_linvel(vel, true);
                }
                PhysicsMessage::MyPaddle(paddle_y) => {
                    let my_paddle_body = rigid_body_set.get_mut(
                        rigid_bodies.my_paddle_body_handle
                    ).unwrap();
                    my_paddle_body.set_translation(vector![MY_PADDLE_X, paddle_y], true);
                }
                PhysicsMessage::TheirPaddle(paddle_y) => {
                    let their_paddle_body = rigid_body_set.get_mut(
                        rigid_bodies.their_paddle_body_handle
                    ).unwrap();
                    their_paddle_body.set_translation(vector![THEIR_PADDLE_X, paddle_y], true);
                }
                PhysicsMessage::Ball(ball) => {
                    let ball_body = rigid_body_set.get_mut(
                        rigid_bodies.ball_body_handle
                    ).unwrap();
                    ball_body.set_position(ball.position, true);
                    ball_body.set_linvel(ball.linvel, true);
                    ball_body.set_angvel(ball.angvel, true);
                }
            }
        }


        for collision in collision_recv.try_iter() {
            if let CollisionEvent::Started(collider1, collider2, _) = collision {
                if collider1 == colliders.border_left_collider_handle
                    || collider2 == colliders.border_left_collider_handle {
                    send_physics_message.send(PhysicsMessage::StartGame).unwrap();
                    graphics.send_message(Msg::IScored);
                } else if collider1 == colliders.border_right_collider_handle
                    || collider2 == colliders.border_right_collider_handle {
                    send_physics_message.send(PhysicsMessage::StartGame).unwrap();
                    graphics.send_message(Msg::TheyScored);
                }
            }
        }


        let timestamp = timestamp.as_f64().unwrap() as f32;
        // Change in time since last animation frame
        let dt = {
            let timestamp = timestamp / 1000.0;
            if start_time.is_none() {
                // initialize timestamp
                start_time = Some(timestamp);
            }
            // get the diff since last run
            let diff = timestamp - start_time.unwrap();
            // set to current time for next cycle
            start_time = Some(timestamp);
            diff
        };

        // use the timestamp diff from the animation frame request
        let integration_parameters = IntegrationParameters {
            dt,
            ..IntegrationParameters::default()
        };

        // step physics engine
        physics_pipeline.step(
            &gravity,
            &integration_parameters,
            &mut island_manager,
            &mut broad_phase,
            &mut narrow_phase,
            &mut rigid_body_set,
            &mut collider_set,
            &mut impulse_joint_set,
            &mut multibody_joint_set,
            &mut ccd_solver,
            &(),
            &event_handler,
        );

        // Emit changes to dynamic entities in graphics
        let mut needs_to_render = false;
        // // TODO how could this work with networking events, like another player, rather than physics events?
        if island_manager.active_kinematic_bodies().len() > 0 {
            needs_to_render = true;
        }
        if island_manager.active_dynamic_bodies().len() > 0 {
            needs_to_render = true;
        }
        let get_paddle_y = |handle: RigidBodyHandle| -> Real {
            rigid_body_set.get(handle).unwrap().translation().y
        };
        let computed = Computed {
            my_paddle: get_paddle_y(rigid_bodies.my_paddle_body_handle),
            their_paddle: get_paddle_y(rigid_bodies.their_paddle_body_handle),
            ball: {
                let ball_rigid_body = rigid_body_set.get(rigid_bodies.ball_body_handle).unwrap();
                BallPositionAndVelocity {
                    position: *ball_rigid_body.position(),
                    linvel: *ball_rigid_body.linvel(),
                    angvel: ball_rigid_body.angvel(),
                }
            },
        };
        if needs_to_render && computed != computed_state {
            computed_state = computed;
            graphics.send_message(Msg::GotComputed(computed));
        }


        // Register next iteration of loop
        let _ = window_cb
            .request_animation_frame(cb_inner.borrow().as_ref().unwrap().as_ref().unchecked_ref())
            .unwrap();
    });
    let cb_closure = Closure::wrap(cb);
    *cb_outer.borrow_mut() = Some(cb_closure);
    // Register first iteration of loop
    let _ = window
        .request_animation_frame(cb_outer.borrow().as_ref().unwrap().as_ref().unchecked_ref())
        .unwrap();

}
