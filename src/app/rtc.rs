use crate::app::host::ping::Ping;

use serde::{Serialize, Deserialize};
use rapier2d::prelude::*;
use num_complex::Complex;
use nalgebra::Unit;
use yew::prelude::Properties;


#[derive(PartialEq, Copy, Clone, Debug, Serialize, Deserialize, Properties)]
pub struct BallPositionAndVelocity {
    pub position: Isometry<Real>,
    pub linvel: Vector<Real>,
    pub angvel: Real,
}

impl BallPositionAndVelocity {
    pub fn new() -> Self {
        Self {
            position: Isometry {
                rotation: Unit::new_normalize(
                    Complex::new(0.0, 0.0)
                ),
                translation: Translation::from(
                   vector![0.0,0.0]
                ),
            },
            linvel: vector![0.0,0.0],
            angvel: 0.0,
        }
    }

    pub fn invert_x(&self) -> Self {
        Self {
            position: Isometry {
                translation: Translation::from(
                    self.position.translation.vector.component_mul(&vector![-1.0, 1.0])
                ),
                ..self.position
            },
            ..*self
        }
    }
}


#[derive(PartialEq, Copy, Clone, Debug, Serialize, Deserialize)]
pub enum RtcMessage {
    Ping(Ping),
    MovingPaddle(Real),
    MovingBall(BallPositionAndVelocity),
    MyScore(u32),
    TheirScore(u32),
}
