use web_sys::window;
use std::collections::HashMap;


enum PingState {
    Waiting(f64), // in absolute time
    Complete(f64), // in difference of time
}

impl PingState {
    fn to_complete(&self) -> Option<f64> {
        match self {
            PingState::Complete(c) => Some(*c),
            _ => None,
        }
    }
}

// Based on the max size of a message - the ball position and velocity - Isometry (4b) + Vector (2b) + angvel (1b) ~ 8b = u64
pub type Ping = u64;

pub struct RtcMessageState {
    next_ping: Ping,
    buffer: HashMap<Ping, PingState>,
}

impl RtcMessageState {
    pub fn new() -> Self {
        Self {
            next_ping: 0,
            buffer: HashMap::with_capacity(10),
        }
    }

    pub fn make_ping(&mut self) -> Ping {
        let ping = self.next_ping;
        self.next_ping = self.next_ping.wrapping_add(1);
        let now = window().unwrap().performance().unwrap().now();
        let _ = self.buffer.insert(ping, PingState::Waiting(now));
        let ping_to_remove = ping.wrapping_sub(10);
        let _ = self.buffer.remove(&ping_to_remove);
        ping
    }

    pub fn load_ping(&mut self, ping: Ping) -> Option<f64> {
        match self.buffer.get(&ping) {
            None => {
                None
            }
            Some(ping_state) => match ping_state {
                PingState::Complete(_) => {
                    None
                }
                PingState::Waiting(then) => {
                    let now = window().unwrap().performance().unwrap().now();
                    let diff = now - then;
                    let _ = self.buffer.insert(ping, PingState::Complete(diff));
                    Some(diff)
                }
            }
        }
    }


    pub fn all_pings(&self) -> Vec<f64> {
        self.buffer.values().filter_map(|p| p.to_complete()).collect()
    }

    pub fn average_ping(&self) -> f64 {
        let completes = self.all_pings();
        completes.iter().sum::<f64>() / (completes.len() as f64)
    }
}
