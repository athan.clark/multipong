pub mod ping;

use ping::RtcMessageState;
use crate::app::rtc::{RtcMessage, BallPositionAndVelocity};
use crate::app::game::Game;
use crate::app::qr::{view_qr_code, QrScanner};

use yew::prelude::*;
use js_sys::{ArrayBuffer, Uint8Array, JSON};
use web_sys::{
    Event,
    HtmlInputElement,
    RtcSessionDescription,
    RtcSessionDescriptionInit,
    MessageEvent,
    RtcDataChannelEvent,
    RtcDataChannel,
    RtcPeerConnection,
    RtcDataChannelType,
    window,
};
use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use log::{info, warn};
use std::cell::RefCell;
use std::rc::Rc;
use rapier2d::prelude::*;


#[derive(PartialEq, Properties)]
pub struct Props {
    pub conn: RtcPeerConnection,
    pub my_answer: Option<Vec<u8>>,
    pub my_paddle: Real,
    pub their_paddle: Real,
    pub ball: BallPositionAndVelocity, // Vector<Real>,
    pub my_score: u32,
    pub their_score: u32,
    pub onchangedmypaddle: Callback<Real>, // Only reflects to physics engine
    pub onchangedtheirpaddle: Callback<Real>,
    pub onconnected: Callback<()>,
}

pub enum Msg {
    GotDataChannel(RtcDataChannel),
    ChangeOffer(String),
    SetRD,
    SetConnected(bool),
    ChangeMyPaddle(Real),
    NoOp,
    Focus,
    StartScanning,
}

#[derive(Clone, PartialEq)]
pub struct RequestOfferCreateAnswer {
    their_offer: String,
    dc: Option<RtcDataChannel>,
    connected: bool,
    my_score: u32,
    their_score: u32,
    ld_ref: NodeRef,
}

impl Component for RequestOfferCreateAnswer {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        let link_ondatachannel = ctx.link().clone();
        let onchangedtheirpaddle_ondatachannel = ctx.props().onchangedtheirpaddle.clone();
        let ondatachannel_cb: Box<dyn Fn(JsValue)> = Box::new(move |e| {
            let e: RtcDataChannelEvent = e.unchecked_into();
            let dc = e.channel();

            // Create a data channel over the peer connection
            dc.set_binary_type(RtcDataChannelType::Arraybuffer); // Use arraybuffer rather than blob


            // When the datachannel opens, start the game
            let link_onopen = link_ondatachannel.clone();
            let onopen_cb: Box<dyn Fn()> = Box::new(move || {
                info!("Opened DataChannel");
                link_onopen.send_message(Msg::SetConnected(true));
            });
            let onopen_cb = Closure::wrap(onopen_cb);
            dc.set_onopen(Some(&onopen_cb.as_ref().unchecked_ref()));
            onopen_cb.forget();

            // Ping issuer
            let message_state = Rc::new(RefCell::new(RtcMessageState::new()));
            let message_state_ping = message_state.clone();
            let dc_ping = dc.clone();
            let ping_cb: Box<dyn Fn()> = Box::new(move || {
                let ping_msg = (*message_state_ping.borrow_mut()).make_ping();
                dc_ping.send_with_u8_array(&bincode::serialize(&RtcMessage::Ping(ping_msg)).unwrap()).unwrap();
            });
            let ping_cb = Closure::wrap(ping_cb);
            let pinger_handle = window().unwrap().set_interval_with_callback_and_timeout_and_arguments_0(
                &ping_cb.as_ref().unchecked_ref(),
                1000 // every second
            ).unwrap();
            ping_cb.forget();

            // When the datachannel closes, end the game
            let link_onclose = link_ondatachannel.clone();
            let onclose_cb: Box<dyn Fn()> = Box::new(move || {
                window().unwrap().clear_interval_with_handle(pinger_handle);
                info!("Closed DataChannel");
                link_onclose.send_message(Msg::SetConnected(false));
            });
            let onclose_cb = Closure::wrap(onclose_cb);
            dc.set_onclose(Some(&onclose_cb.as_ref().unchecked_ref()));
            onclose_cb.forget();

            // End the game and throw the error when there is one
            let onerror_cb: Box<dyn Fn(JsValue)> = Box::new(|e| {
                info!("DataChannel error: {:?}", e);
            });
            let onerror_cb = Closure::wrap(onerror_cb);
            dc.set_onerror(Some(&onerror_cb.as_ref().unchecked_ref()));
            onerror_cb.forget();


            // Relay message
            let onchangedtheirpaddle = onchangedtheirpaddle_ondatachannel.clone();
            let onmessage_cb: Box<dyn FnMut(MessageEvent)> = Box::new(move |e| {
                let ab = e.data().unchecked_into::<ArrayBuffer>();
                let byte_array = Uint8Array::new(&ab);
                let mut bytes = vec![0u8; byte_array.byte_length() as usize];
                byte_array.copy_to(&mut bytes);
                match bincode::deserialize(&bytes) {
                    Ok(message) => {
                        match message {
                            RtcMessage::MovingPaddle(mp) => {
                                onchangedtheirpaddle.emit(mp);
                            }
                            RtcMessage::Ping(ping_id) => {
                                {
                                    (*message_state.borrow_mut()).load_ping(ping_id);
                                }
                                let all_pings = (*message_state.borrow()).all_pings();
                                let avg_ping = (*message_state.borrow()).average_ping();
                            }
                            _ => {} // The host is the authority for ball position
                        }
                    }
                    Err(e) => {
                        warn!("Couldn't decode message: {:?}", e);
                    }
                }
            });
            let onmessage_cb = Closure::wrap(onmessage_cb);
            dc.set_onmessage(Some(&onmessage_cb.as_ref().unchecked_ref()));
            onmessage_cb.forget();

            link_ondatachannel.send_message(Msg::GotDataChannel(dc));
        });
        let ondatachannel_cb = Closure::wrap(ondatachannel_cb);
        ctx.props().conn.set_ondatachannel(Some(&ondatachannel_cb.as_ref().unchecked_ref()));
        ondatachannel_cb.forget();


        // FIXME there needs to be some generalization for the application where the host is the
        // only one that relays messages to the client in regards to physics engine output.
        // Ideally, we'd like the application to be a single component (physics baked-in),
        // and it has a callback fn that gets run every time there's a collision or every 10th of
        // a second to update the client on physics. The client will just provide a no-op, or None.

        let props = ctx.props();

        Self {
            their_offer: "".to_owned(),
            dc: None,
            connected: false,
            my_score: props.my_score,
            their_score: props.their_score,
            ld_ref: NodeRef::default(),
        }
    }

    fn changed(&mut self, ctx: &Context<Self>) -> bool {
        let props = ctx.props();
        if let Some(dc) = &self.dc {
            info!("Sending... {:?}", props.ball);
            dc.send_with_u8_array(
                &bincode::serialize(&RtcMessage::MovingBall(
                    props.ball.invert_x()
                )).unwrap()
            ).unwrap();
        }
        if self.my_score != props.my_score {
            self.my_score = props.my_score;
            if let Some(dc) = &self.dc {
                dc.send_with_u8_array(
                    &bincode::serialize(&RtcMessage::TheirScore(self.my_score)).unwrap()
                ).unwrap();
            }
        }
        if self.their_score != props.their_score {
            self.their_score = props.their_score;
            if let Some(dc) = &self.dc {
                dc.send_with_u8_array(
                    &bincode::serialize(&RtcMessage::MyScore(self.their_score)).unwrap()
                ).unwrap();
            }
        }
        true
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::GotDataChannel(dc) => {
                self.dc = Some(dc);
                false
            }
            Msg::ChangeOffer(offer) => {
                self.their_offer = offer;
                true
            }
            Msg::SetRD => {
                let rd = z85::decode(self.their_offer.clone()).unwrap();
                let (rd, _csum) = yazi::decompress(&rd, yazi::Format::Zlib).unwrap(); // FIXME handle errors
                let rd = std::str::from_utf8(&rd).unwrap();
                let rd = JSON::parse(rd).unwrap();
                let rd: RtcSessionDescriptionInit = rd.unchecked_into();
                let conn = ctx.props().conn.clone();
                let _ = conn.set_remote_description(&rd);

                let conn_handle_answer = conn.clone();
                let handle_answer_cb: Box<dyn FnMut(JsValue)> = Box::new(move |sd| {
                    let sd: RtcSessionDescription = sd.unchecked_into();
                    // Causes an ICECandidate event
                    // https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/icecandidate_event
                    let _ = conn_handle_answer.set_local_description(
                        &sd.unchecked_into::<RtcSessionDescriptionInit>()
                    );
                });
                let handle_answer_cb = Closure::wrap(handle_answer_cb);
                let _ = conn.create_answer().then(&handle_answer_cb);
                handle_answer_cb.forget();
                false
            }
            Msg::SetConnected(connected) => {
                self.connected = connected;
                if connected {
                    ctx.props().onconnected.emit(());
                }
                true
            }
            Msg::ChangeMyPaddle(paddle) => {
                ctx.props().onchangedmypaddle.emit(paddle);
                if let Some(dc) = &self.dc {
                    dc.send_with_u8_array(
                        &bincode::serialize(&RtcMessage::MovingPaddle(paddle)).unwrap()
                    ).unwrap();
                }
                false
            }
            Msg::NoOp => {
                false
            }
            Msg::Focus => {
                if let Some(e) = self.ld_ref.cast::<HtmlInputElement>() {
                    e.select();
                }
                false
            }
            Msg::StartScanning => {
                let onscan: Box<dyn FnMut(JsValue)> = Box::new(|data| {
                    info!("Got scan result: {:?}", data);
                });
                let onscan = Closure::wrap(onscan);
                let qr_scanner = QrScanner::new(&onscan.as_ref().unchecked_ref());
                onscan.forget();
                qr_scanner.start();
                false
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let props = ctx.props();
        let get_connected = if self.connected {
            html! {<></>}
        } else {
            // let qr_code = match &props.my_answer {
            //     None => html! { <></> },
            //     Some(my_answer) => view_qr_code(my_answer),
            // };
            html! {
                <div style="color:white">
                    <h2>{"Offer"}</h2>
                    <input
                        type="text"
                        onchange={link.callback(|e: Event|
                            Msg::ChangeOffer(
                                e.target_unchecked_into::<HtmlInputElement>().value()
                            )
                        )} />
                    <button onclick={link.callback(|_| Msg::SetRD)}>{"Set Offer"}</button>
                    // <video id="qr-scanner"></video>
                    // <button onclick={link.callback(|_| Msg::StartScanning)}>{"Scan QR Code"}</button>
                    <h2>{"Answer"}</h2>
                    <input
                        type="text"
                        ref={self.ld_ref.clone()}
                        onchange={link.callback(|_| Msg::NoOp)}
                        onfocus={link.callback(|_| Msg::Focus)}
                        value={props.my_answer.clone().map(z85::encode).unwrap_or("".to_owned())} />
                    // {qr_code}
                </div>
            }
        };
        let game = if self.connected {
            html! {
                <Game
                    onchangedmypaddle={link.callback(Msg::ChangeMyPaddle)}
                    my_paddle={props.my_paddle}
                    their_paddle={props.their_paddle}
                    ball={props.ball.position.translation.vector}
                    my_score={props.my_score}
                    their_score={props.their_score} />
            }
        } else {
            html! {<></>}
        };
        html! {
            <>
                {get_connected}
                {game}
            </>
        }
    }
}
