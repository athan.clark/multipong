use crate::app::rtc::{RtcMessage, BallPositionAndVelocity};
use crate::app::game::Game;
use crate::app::qr::view_qr_code;

use yew::prelude::*;
use js_sys::{ArrayBuffer, Uint8Array, JSON};
use web_sys::{
    Event,
    HtmlInputElement,
    RtcSessionDescriptionInit,
    MessageEvent,
    RtcDataChannel,
    RtcDataChannelInit,
    RtcPeerConnection,
    RtcDataChannelType,
};
use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use rapier2d::prelude::*;
use log::{info, warn};


#[derive(PartialEq, Properties)]
pub struct Props {
    pub conn: RtcPeerConnection,
    pub my_offer: Option<Vec<u8>>,
    pub my_paddle: Real,
    pub their_paddle: Real,
    pub ball: Vector<Real>,
    pub my_score: u32,
    pub their_score: u32,
    pub onchangedmypaddle: Callback<Real>, // Only reflects up to physics engine
    pub onchangedtheirpaddle: Callback<Real>,
    pub onchangedball: Callback<BallPositionAndVelocity>,
    pub onsetmyscore: Callback<u32>,
    pub onsettheirscore: Callback<u32>,
}

pub enum Msg {
    ChangeAnswer(String),
    SetRD,
    SetConnected(bool),
    ChangeMyPaddle(Real),
    NoOp,
    Focus,
}

#[derive(Clone, PartialEq)]
pub struct CreateOfferRequestAnswer {
    their_answer: String,
    dc: RtcDataChannel,
    connected: bool,
    ld_ref: NodeRef,
}

impl Component for CreateOfferRequestAnswer {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        let conn = ctx.props().conn.clone();

        // Forget any datachannel handler
        conn.set_ondatachannel(None);

        // Create a data channel over the peer connection
        let mut dc_init = RtcDataChannelInit::new();
        let dc_init = dc_init.ordered(false);
        let dc = conn.create_data_channel_with_data_channel_dict("pong", &dc_init);
        dc.set_binary_type(RtcDataChannelType::Arraybuffer); // Use arraybuffer rather than blob


        // When the datachannel opens, start the game
        let link_onopen = ctx.link().clone();
        let onopen_cb: Box<dyn Fn()> = Box::new(move || {
            info!("Opened DataChannel");
            link_onopen.send_message(Msg::SetConnected(true));
        });
        let onopen_cb = Closure::wrap(onopen_cb);
        dc.set_onopen(Some(&onopen_cb.as_ref().unchecked_ref()));
        onopen_cb.forget();

        // When the datachannel closes, end the game
        let link_onclose = ctx.link().clone();
        let onclose_cb: Box<dyn Fn()> = Box::new(move || {
            info!("Closed DataChannel");
            link_onclose.send_message(Msg::SetConnected(false));
        });
        let onclose_cb = Closure::wrap(onclose_cb);
        dc.set_onclose(Some(&onclose_cb.as_ref().unchecked_ref()));
        onclose_cb.forget();

        // End the game and throw the error when there is one
        let onerror_cb: Box<dyn Fn(JsValue)> = Box::new(|e| {
            info!("DataChannel error: {:?}", e);
        });
        let onerror_cb = Closure::wrap(onerror_cb);
        dc.set_onerror(Some(&onerror_cb.as_ref().unchecked_ref()));
        onerror_cb.forget();

        // Relay message
        let dc_ping = dc.clone();
        let onchangedtheirpaddle = ctx.props().onchangedtheirpaddle.clone();
        let onchangedball = ctx.props().onchangedball.clone();
        let onsetmyscore = ctx.props().onsetmyscore.clone();
        let onsettheirscore = ctx.props().onsettheirscore.clone();
        let onmessage_cb: Box<dyn FnMut(MessageEvent)> = Box::new(move |e| {
            let ab = e.data().unchecked_into::<ArrayBuffer>();
            let byte_array = Uint8Array::new(&ab);
            let mut bytes = vec![0u8; byte_array.byte_length() as usize];
            byte_array.copy_to(&mut bytes);
            match bincode::deserialize(&bytes) {
                Ok(message) => {
                    match message {
                        RtcMessage::MovingPaddle(mp) => {
                            // send velocity and position to physics engine, let physics engine's step
                            // per requestAnimationFrame send messages to the graphics engine
                            onchangedtheirpaddle.emit(mp);
                        }
                        RtcMessage::MovingBall(mb) => {
                            onchangedball.emit(mb);
                        }
                        RtcMessage::MyScore(my_score) => {
                            onsetmyscore.emit(my_score);
                        }
                        RtcMessage::TheirScore(their_score) => {
                            onsettheirscore.emit(their_score);
                        }
                        ping_msg@RtcMessage::Ping(_) => {
                            // Reflect pings back to the host asap
                            dc_ping.send_with_u8_array(&bincode::serialize(&ping_msg).unwrap()).unwrap();
                        }
                    }
                }
                Err(e) => {
                    warn!("Couldn't decode message: {:?}", e);
                }
            }
        });
        let onmessage_cb = Closure::wrap(onmessage_cb);
        dc.set_onmessage(Some(&onmessage_cb.as_ref().unchecked_ref()));
        onmessage_cb.forget();

        Self {
            their_answer: "".to_owned(),
            dc,
            connected: false,
            ld_ref: NodeRef::default(),
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::ChangeAnswer(answer) => {
                self.their_answer = answer;
                true
            }
            Msg::SetRD => {
                let rd = z85::decode(self.their_answer.clone()).unwrap();
                let (rd, _csum) = yazi::decompress(&rd, yazi::Format::Zlib).unwrap(); // FIXME handle errors
                let rd = std::str::from_utf8(&rd).unwrap();
                let rd = JSON::parse(rd).unwrap();
                let rd: RtcSessionDescriptionInit = rd.unchecked_into();
                let _ = ctx.props().conn.set_remote_description(&rd);
                false
            }
            Msg::SetConnected(connected) => {
                self.connected = connected;
                true
            }
            Msg::ChangeMyPaddle(paddle) => {
                ctx.props().onchangedmypaddle.emit(paddle);
                self.dc.send_with_u8_array(
                    &bincode::serialize(&RtcMessage::MovingPaddle(paddle)).unwrap()
                ).unwrap();
                false
            }
            Msg::NoOp => {
                false
            }
            Msg::Focus => {
                if let Some(e) = self.ld_ref.cast::<HtmlInputElement>() {
                    e.select();
                }
                false
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let props = ctx.props();
        let get_connected = if self.connected {
            html! {<></>}
        } else {
            // let qr_code = match &props.my_offer {
            //     None => html! { <></> },
            //     Some(my_offer) => view_qr_code(my_offer),
            // };
            html! {
                <div style="color:white">
                    <h2>{"Offer"}</h2>
                    <input
                        type="text"
                        ref={self.ld_ref.clone()}
                        onchange={link.callback(|_| Msg::NoOp)}
                        onfocus={link.callback(|_| Msg::Focus)}
                        value={props.my_offer.clone().map(z85::encode).unwrap_or("".to_owned())} />
                    // {qr_code}
                    <h2>{"Answer"}</h2>
                    <input
                        type="text"
                        onchange={link.callback(|e: Event|
                            Msg::ChangeAnswer(
                                e.target_unchecked_into::<HtmlInputElement>().value()
                            )
                        )} />
                    <button onclick={link.callback(|_| Msg::SetRD)}>{"Set Answer"}</button>
                </div>
            }
        };
        let game = if self.connected {
            html! {
                <Game
                    onchangedmypaddle={link.callback(Msg::ChangeMyPaddle)}
                    my_paddle={props.my_paddle}
                    their_paddle={props.their_paddle}
                    ball={props.ball}
                    my_score={props.my_score}
                    their_score={props.their_score} />
            }
        } else {
            html! {<></>}
        };
        html! {
            <>
                {get_connected}
                {game}
            </>
        }
    }
}
