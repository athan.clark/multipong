use crate::app::physics::{
    PADDLE_HALF_HEIGHT,
    PADDLE_HALF_WIDTH,
    MY_PADDLE_X,
    THEIR_PADDLE_X,
    BALL_RADIUS,
    WINDOW_HALF_HEIGHT,
    WINDOW_HALF_WIDTH,
};

use yew::prelude::*;
use rapier2d::prelude::*;
use web_sys::{window, MouseEvent, Element};
use wasm_bindgen::JsCast;


#[derive(PartialEq, Properties)]
pub struct Props {
    pub onchangedmypaddle: Callback<Real>, // Can only relay that I've moved my paddle
    pub my_paddle: Real,
    pub their_paddle: Real,
    pub ball: Vector<Real>,
    pub my_score: u32,
    pub their_score: u32,
}

pub enum Msg {
    MovedMouse(i32),
    NoOp,
}

pub struct Game;

impl Component for Game {
    type Message = Msg;
    type Properties = Props;

    fn create(_ctx: &Context<Self>) -> Self {
        Self
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::NoOp => {
                false
            }
            Msg::MovedMouse(offset_y) => {
                let offset_y = offset_y as Real;
                let window_y = window().unwrap()
                    .document().unwrap()
                    .get_element_by_id("app").unwrap()
                    .client_height() as Real;
                let portion_of_window = offset_y / window_y;
                ctx.props().onchangedmypaddle.emit((portion_of_window - 0.5) * -(WINDOW_HALF_HEIGHT * 2.0));
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        let link = ctx.link();

        fn draw_paddle(pos: Vector<Real>) -> Html {
            html! {
                <rect
                    x={(pos.x - PADDLE_HALF_WIDTH).to_string()}
                    y={(pos.y - PADDLE_HALF_HEIGHT).to_string()}
                    width={(PADDLE_HALF_WIDTH * 2.0).to_string()}
                    height={(PADDLE_HALF_HEIGHT * 2.0).to_string()} />
            }
        }

        let my_paddle: Html = {
            let my_paddle_pos: Vector<Real> = point_to_svg(vector![MY_PADDLE_X, props.my_paddle]);
            draw_paddle(my_paddle_pos)
        };
        let their_paddle: Html = {
            let their_paddle_pos: Vector<Real> = point_to_svg(vector![THEIR_PADDLE_X, props.their_paddle]);
            draw_paddle(their_paddle_pos)
        };
        let ball: Html = {
            let ball_pos: Vector<Real> = point_to_svg(props.ball);
            html! {
                <circle cx={ball_pos.x.to_string()} cy={ball_pos.y.to_string()} r={BALL_RADIUS.to_string()} />
            }
        };

        fn get_touch_y(e: TouchEvent) -> Option<i32> {
            let ev: Event = e.clone().unchecked_into();
            ev.prevent_default();
            let rect = e.target_unchecked_into::<Element>().get_bounding_client_rect();
            e.target_touches().get(0).map(|t| t.page_y() - rect.top().floor() as i32)
        }

        html! {
            <svg
                id="app"
                viewBox={format!("0 0 {} {}", WINDOW_HALF_WIDTH * 2.0, WINDOW_HALF_HEIGHT * 2.0)}
                xmlns="http://www.w3.org/2000/svg"
                ontouchstart={link.callback(|e: TouchEvent| match get_touch_y(e) {
                    None => Msg::NoOp,
                    Some(y) => Msg::MovedMouse(y)
                })}
                ontouchmove={link.callback(|e: TouchEvent| match get_touch_y(e) {
                    None => Msg::NoOp,
                    Some(y) => Msg::MovedMouse(y)
                })}
                onmousemove={link.callback(|e: MouseEvent| Msg::MovedMouse(e.offset_y()))} >
                {my_paddle}
                {their_paddle}
                {ball}
                <text
                    x={(WINDOW_HALF_WIDTH * 3.0 / 2.0).to_string()}
                    y={(WINDOW_HALF_HEIGHT / 3.0).to_string()}
                    text-anchor="middle"
                    font-size="10%"
                    >
                    {props.my_score}
                </text>
                <text
                    x={(WINDOW_HALF_WIDTH * 1.0 / 2.0).to_string()}
                    y={(WINDOW_HALF_HEIGHT / 3.0).to_string()}
                    text-anchor="middle"
                    font-size="10%"
                    >
                    {props.their_score}
                </text>
            </svg>
        }
    }
}


fn point_to_svg(point: Vector<Real>) -> Vector<Real> {
    (point + vector![WINDOW_HALF_WIDTH, WINDOW_HALF_HEIGHT]).component_mul(&vector![1.0, -1.0])
        + vector![0.0, WINDOW_HALF_HEIGHT * 2.0]
}

fn svg_to_point(point: Vector<Real>) -> Vector<Real> {
    (point - vector![0.0, WINDOW_HALF_HEIGHT * 2.0]).component_mul(&vector![1.0, -1.0])
        - vector![WINDOW_HALF_WIDTH, WINDOW_HALF_HEIGHT]
}
