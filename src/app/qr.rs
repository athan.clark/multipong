use yew::prelude::*;
use qrcodegen::{QrCode, QrCodeEcc};
use web_sys::window;
use js_sys::{Function, Array, Reflect};
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::{JsValue, JsCast};


pub fn view_qr_code(data: &[u8]) -> Html {
    match QrCode::encode_binary(data, QrCodeEcc::High) {
        Err(e) => html! {
            <p>{format!("QR Code error: {:?}", e)}</p>
        },
        Ok(q) => {
            let size: i32 = q.size();
            fn view_segment(x: i32, y: i32, v: bool) -> Html {
                let fill = if v {"#000"} else {"#fff"};
                html! {
                    <rect
                        x={x.to_string()}
                        y={y.to_string()}
                        width="1"
                        height="1"
                        style={format!("fill:{}", fill)} />
                }
            }
            let content: Html = {
                (0..size).map(|y|
                    (0..size).map(|x|
                        view_segment(x, y, q.get_module(x,y))
                    ).collect::<Html>()
                ).collect()
            };
            html! {
                <svg viewBox={format!("0 0 {} {}", size, size)}>
                    {content}
                </svg>
            }
        }
    }
}


pub struct QrScanner {
    qr_scanner: JsValue
}

impl QrScanner {
    pub fn new(onresult: &Function) -> Self {
        let video = window().unwrap().document().unwrap().get_element_by_id("qr-scanner").unwrap();
        let mut args = Array::new();
        args.push(&video);
        args.push(&onresult);
        Self {
            qr_scanner: Reflect::construct(&QR_SCANNER, &args).unwrap(),
        }
    }

    pub fn start(&self) {
        Reflect::get(&self.qr_scanner, &JsValue::from_str("start"))
            .unwrap()
            .unchecked_into::<Function>()
            .call0(&self.qr_scanner);
    }
}


#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_name = "QrScanner")]
    static QR_SCANNER: Function;
}
