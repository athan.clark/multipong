use crate::app::rtc::BallPositionAndVelocity;

use rapier2d::prelude::*;
use std::ops::Neg;


pub static WINDOW_HALF_HEIGHT: Real = 9.0;
pub static WINDOW_HALF_WIDTH: Real = 16.0;
pub static PADDLE_HALF_HEIGHT: Real = 2.0;
pub static PADDLE_HALF_WIDTH: Real = 0.5;
pub static MY_PADDLE_X: Real = WINDOW_HALF_WIDTH - PADDLE_HALF_WIDTH;
pub static THEIR_PADDLE_X: Real = (WINDOW_HALF_WIDTH * -1.0) + PADDLE_HALF_WIDTH;
pub static BALL_RADIUS: Real = 0.5;
pub static WALL_WIDTH: Real = 1.0;


pub struct RigidBodyHandles {
    pub my_paddle_body_handle: RigidBodyHandle,
    pub their_paddle_body_handle: RigidBodyHandle,
    pub ball_body_handle: RigidBodyHandle,
}

pub struct ColliderHandles {
    pub my_paddle_collider_handle: ColliderHandle,
    pub their_paddle_collider_handle: ColliderHandle,
    pub ball_collider_handle: ColliderHandle,
    pub border_left_collider_handle: ColliderHandle,
    pub border_right_collider_handle: ColliderHandle,
}

pub struct Scene {
    pub rigid_bodies: RigidBodyHandles,
    pub colliders: ColliderHandles,
}

impl Scene {
    pub fn initialize(
        rigid_body_set: &mut RigidBodySet,
        collider_set: &mut ColliderSet,
    ) -> Self {
        // Arena borders
        let border_bottom = ColliderBuilder::cuboid(WINDOW_HALF_WIDTH, WALL_WIDTH)
            .translation(vector![0.0, WINDOW_HALF_HEIGHT.neg() - WALL_WIDTH])
            .friction(0.0)
            .restitution(1.0)
            .build();
        collider_set.insert(border_bottom);
        let border_top = ColliderBuilder::cuboid(WINDOW_HALF_WIDTH, WALL_WIDTH)
            .translation(vector![0.0, WINDOW_HALF_HEIGHT + WALL_WIDTH])
            .friction(0.0)
            .restitution(1.0)
            .build();
        collider_set.insert(border_top);
        let border_left = ColliderBuilder::cuboid(WALL_WIDTH, WINDOW_HALF_HEIGHT)
            .translation(vector![WINDOW_HALF_WIDTH.neg() - WALL_WIDTH, 0.0])
            .friction(0.0)
            .restitution(1.0)
            .build();
        let border_left_collider_handle = collider_set.insert(border_left);
        let border_right = ColliderBuilder::cuboid(WALL_WIDTH, WINDOW_HALF_HEIGHT)
            .translation(vector![WINDOW_HALF_WIDTH + WALL_WIDTH, 0.0])
            .friction(0.0)
            .restitution(1.0)
            .build();
        let border_right_collider_handle = collider_set.insert(border_right);

        // My paddle
        let my_paddle_rigid_body = RigidBodyBuilder::kinematic_position_based()
            .translation(vector![MY_PADDLE_X, 0.0]) // My paddle is on the right
            .gravity_scale(0.0)
            .lock_rotations()
            .build();
        let my_paddle_collider = ColliderBuilder::cuboid(PADDLE_HALF_WIDTH, PADDLE_HALF_HEIGHT)
            .friction(0.5)
            .restitution(1.5)
            .active_events(ActiveEvents::COLLISION_EVENTS)
            // .user_data(EntityType::My_Paddle.to_user_data(vec![])) // FIXME is this needed?
            .build();
        let my_paddle_body_handle: RigidBodyHandle = rigid_body_set.insert(my_paddle_rigid_body);
        let my_paddle_collider_handle = collider_set.insert_with_parent(
            my_paddle_collider,
            my_paddle_body_handle,
            rigid_body_set,
        );

        // Enemy's paddle
        let their_paddle_rigid_body = RigidBodyBuilder::kinematic_position_based()
            .translation(vector![THEIR_PADDLE_X, 0.0]) // Their paddle is on the left
            .gravity_scale(0.0)
            .lock_rotations()
            .build();
        let their_paddle_collider = ColliderBuilder::cuboid(PADDLE_HALF_WIDTH, PADDLE_HALF_HEIGHT)
            .friction(0.5)
            .restitution(1.5)
            .active_events(ActiveEvents::COLLISION_EVENTS)
        // .user_data(EntityType::Their_Paddle.to_user_data(vec![])) // FIXME is this needed?
            .build();
        let their_paddle_body_handle: RigidBodyHandle = rigid_body_set.insert(their_paddle_rigid_body);
        let their_paddle_collider_handle = collider_set.insert_with_parent(
            their_paddle_collider,
            their_paddle_body_handle,
            rigid_body_set,
        );

        // The ball bouncing around
        let ball_rigid_body = RigidBodyBuilder::dynamic()
            .translation(vector![0.0, 0.0])
            .linear_damping(0.0)
            .angular_damping(0.0)
            .gravity_scale(0.0)
            .build();
        let ball_collider = ColliderBuilder::ball(BALL_RADIUS)
            .friction(0.0)
            .restitution(1.0)
            .active_events(ActiveEvents::COLLISION_EVENTS)
        // .user_data(EntityType::Ball.to_user_data(vec![])) // FIXME is this needed?
            .build();
        let ball_body_handle: RigidBodyHandle = rigid_body_set.insert(ball_rigid_body);
        let ball_collider_handle = collider_set.insert_with_parent(
            ball_collider,
            ball_body_handle,
            rigid_body_set,
        );



        Self {
            rigid_bodies: RigidBodyHandles {
                my_paddle_body_handle,
                their_paddle_body_handle,
                ball_body_handle,
            },
            colliders: ColliderHandles {
                my_paddle_collider_handle,
                their_paddle_collider_handle,
                ball_collider_handle,
                border_left_collider_handle,
                border_right_collider_handle,
            },
        }
    }
}


pub enum PhysicsMessage {
    MyPaddle(Real),
    TheirPaddle(Real),
    Ball(BallPositionAndVelocity),
    StartGame,
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub struct Computed {
    pub my_paddle: Real,
    pub their_paddle: Real,
    pub ball: BallPositionAndVelocity,
}

impl Computed {
    pub fn new() -> Self {
        Self {
            my_paddle: 0.0,
            their_paddle: 0.0,
            ball: BallPositionAndVelocity::new(),
        }
    }
}
