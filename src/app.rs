mod host;
mod client;
pub mod rtc;
pub mod physics;
mod game;
mod qr;

use host::RequestOfferCreateAnswer;
use client::CreateOfferRequestAnswer;
use physics::{PhysicsMessage, Computed};
use rtc::BallPositionAndVelocity;

use yew::prelude::*;
use web_sys::{
    RtcPeerConnection,
    RtcConfiguration,
    RtcSessionDescription,
    RtcSessionDescriptionInit,
    RtcPeerConnectionIceEvent,
};
use js_sys::{Reflect, Object, Array, JSON};
use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsValue, JsCast};
use rapier2d::prelude::*;


#[derive(PartialEq, Properties)]
pub struct Props {
    pub onphysicsmessage: Callback<PhysicsMessage>,
}

pub enum Msg {
    CreateOfferRequestAnswer,
    RequestOfferCreateAnswer,
    ChangedLD(Vec<u8>),
    ChangedMyPaddle(Real),
    GotTheirPaddle(Real),
    GotBall(BallPositionAndVelocity),
    GotComputed(Computed),
    OnConnected,
    SetMyScore(u32),
    SetTheirScore(u32),
    IScored,
    TheyScored,
}

#[derive(Clone, PartialEq)]
pub struct App {
    conn: RtcPeerConnection,
    is_host: Option<bool>,
    local_description: Option<Vec<u8>>,
    my_paddle: Real,
    their_paddle: Real,
    ball: BallPositionAndVelocity, // Vector<Real>,
    my_score_client: u32,
    their_score_client: u32,
    my_score_host: u32,
    their_score_host: u32,
}


impl Component for App {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        // Configure browser to search with these ICE servers
        let google_ice_server = Object::new();
        Reflect::set(
            &google_ice_server,
            &JsValue::from_str("urls"),
            &JsValue::from_str("stun:stun.l.google.com:19302")
        ).unwrap();

        let ice_servers = Array::new();
        let _ = ice_servers.push(&google_ice_server);

        let mut config = RtcConfiguration::new();
        let config = config.ice_servers(&ice_servers);

        // Create a (currently empty) peer connection
        let conn = RtcPeerConnection::new_with_configuration(&config).unwrap();

        // Load the offer generated by ICE candidates to the peer connection as a local description / offer
        let conn_onnegotiationneeded = conn.clone();
        let onnegotiationneeded_cb: Box<dyn FnMut()> = Box::new(move || {
            let conn_handle_offer = conn_onnegotiationneeded.clone();
            let handle_offer_cb: Box<dyn FnMut(JsValue)> = Box::new(move |sd| {
                let sd: RtcSessionDescription = sd.unchecked_into();
                // Causes an ICECandidate event
                // https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/icecandidate_event
                let _ = conn_handle_offer.set_local_description(
                    &sd.unchecked_into::<RtcSessionDescriptionInit>()
                );
            });
            let handle_offer_cb = Closure::wrap(handle_offer_cb);
            // FIXME are you sure we should create an offer?
            let _ = conn_onnegotiationneeded.create_offer().then(&handle_offer_cb);
            handle_offer_cb.forget()
        });
        let onnegotiationneeded_cb = Closure::wrap(onnegotiationneeded_cb);
        conn.set_onnegotiationneeded(Some(&onnegotiationneeded_cb.as_ref().unchecked_ref()));
        onnegotiationneeded_cb.forget();

        // Relay ICE candidates through the peer connection
        let link = ctx.link().clone();
        let conn_onicecandidate = conn.clone();
        let onicecandidate_cb: Box<dyn FnMut(RtcPeerConnectionIceEvent)> = Box::new(move |e| {
            // Indicates there are no further candidates in this negotiation session (negotiations are completed)
            // https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnectionIceEvent/candidate
            if let None = e.candidate() {
                // Get the local description
                if let Some(ld) = conn_onicecandidate.local_description() {
                    let ld: Vec<u8> = JSON::stringify(&ld.to_json())
                        .unwrap()
                        .as_string()
                        .unwrap()
                        .bytes()
                        .collect();
                    let ld = yazi::compress(&ld, yazi::Format::Zlib, yazi::CompressionLevel::Default).unwrap();
                    // let ld: String = z85::encode(ld);
                    link.send_message(Msg::ChangedLD(ld));
                }
            }
        });
        let onicecandidate_cb = Closure::wrap(onicecandidate_cb);
        conn.set_onicecandidate(Some(&onicecandidate_cb.as_ref().unchecked_ref()));
        onicecandidate_cb.forget();

        Self {
            conn,
            is_host: None,
            local_description: None,
            my_paddle: 0.0,
            their_paddle: 0.0,
            ball: BallPositionAndVelocity::new(),
            my_score_client: 0,
            their_score_client: 0,
            my_score_host: 0,
            their_score_host: 0,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::CreateOfferRequestAnswer => {
                self.is_host = Some(false);
                true
            }
            Msg::RequestOfferCreateAnswer => {
                self.is_host = Some(true);
                true
            }
            Msg::ChangedLD(ld) => {
                self.local_description = Some(ld);
                true
            }
            Msg::ChangedMyPaddle(paddle) => {
                ctx.props().onphysicsmessage.emit(PhysicsMessage::MyPaddle(paddle));
                false
            }
            Msg::GotTheirPaddle(paddle) => {
                ctx.props().onphysicsmessage.emit(PhysicsMessage::TheirPaddle(paddle));
                false
            }
            Msg::GotBall(ball) => {
                ctx.props().onphysicsmessage.emit(PhysicsMessage::Ball(ball));
                false
            }
            Msg::GotComputed(computed) => {
                self.my_paddle = computed.my_paddle;
                self.their_paddle = computed.their_paddle;
                self.ball = computed.ball;
                true
            }
            Msg::OnConnected => {
                ctx.props().onphysicsmessage.emit(PhysicsMessage::StartGame);
                false
            }
            Msg::SetMyScore(my_score) => {
                self.my_score_client = my_score;
                true
            }
            Msg::SetTheirScore(their_score) => {
                self.their_score_client = their_score;
                true
            }
            Msg::IScored => {
                self.my_score_host += 1;
                true
            }
            Msg::TheyScored => {
                self.their_score_host += 1;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        let host_or_client_buttons = match self.is_host {
            None => html! {
                <>
                    <button onclick={link.callback(|_| Msg::CreateOfferRequestAnswer)}>{"Client"}</button>
                    <button onclick={link.callback(|_| Msg::RequestOfferCreateAnswer)}>{"Server"}</button>
                </>
            },
            _ => html! {<></>},
        };

        let host_or_client = match self.is_host {
            Some(is_host) => if is_host {
                    html! {
                        <RequestOfferCreateAnswer
                            conn={self.conn.clone()}
                            my_answer={self.local_description.clone()}
                            my_paddle={self.my_paddle}
                            their_paddle={self.their_paddle}
                            ball={self.ball}
                            my_score={self.my_score_host}
                            their_score={self.their_score_host}
                            onchangedmypaddle={link.callback(Msg::ChangedMyPaddle)}
                            onchangedtheirpaddle={link.callback(Msg::GotTheirPaddle)}
                            onconnected={link.callback(|_| Msg::OnConnected)} />
                    }
                } else {
                    html! {
                        <CreateOfferRequestAnswer
                            conn={self.conn.clone()}
                            my_offer={self.local_description.clone()}
                            my_paddle={self.my_paddle}
                            their_paddle={self.their_paddle}
                            ball={self.ball.position.translation.vector}
                            my_score={self.my_score_client}
                            their_score={self.their_score_client}
                            onchangedmypaddle={link.callback(Msg::ChangedMyPaddle)}
                            onchangedtheirpaddle={link.callback(Msg::GotTheirPaddle)}
                            onchangedball={link.callback(Msg::GotBall)}
                            onsetmyscore={link.callback(Msg::SetMyScore)}
                            onsettheirscore={link.callback(Msg::SetTheirScore)} />
                    }
                },
            _ => html! {<></>},
        };

        html! {
            <>
                {host_or_client_buttons}
                {host_or_client}
            </>
        }
    }
}
